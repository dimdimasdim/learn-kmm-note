package com.learn.learnkmpnote.domain.note

import com.learn.learnkmpnote.domain.time.DateTimeUtils
import kotlin.math.sqrt

class SearchNotes {

    fun execute(notes: List<Note>, query: String): List<Note> {
        if (query.isBlank()) return notes
        return notes.filter {
            it.title.trim().lowercase().contains(query.lowercase()) || it.content.trim()
                .lowercase().contains(query.lowercase())
        }.sortedBy { DateTimeUtils.toEpochMillis(it.created) }
    }

}