package com.learn.learnkmpnote.domain.note

import com.learn.learnkmpnote.presentation.BabyBlueHex
import com.learn.learnkmpnote.presentation.LightGreenHex
import com.learn.learnkmpnote.presentation.RedOrangeHex
import com.learn.learnkmpnote.presentation.RedPinkHex
import com.learn.learnkmpnote.presentation.VioletHex
import kotlinx.datetime.LocalDateTime

data class Note(
    val id: Long?,
    val title: String,
    val content: String,
    val colorHex: Long,
    val created: LocalDateTime
) {

    companion object {
        private val colors = listOf(RedOrangeHex, RedPinkHex, BabyBlueHex, VioletHex, LightGreenHex)

        fun generateColorRandom() = colors.random()
    }
}