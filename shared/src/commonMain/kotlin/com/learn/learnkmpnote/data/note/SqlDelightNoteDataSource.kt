package com.learn.learnkmpnote.data.note

import com.learn.learnkmpnote.database.NoteDatabase
import com.learn.learnkmpnote.domain.note.Note
import com.learn.learnkmpnote.domain.note.NoteDataSource
import com.learn.learnkmpnote.domain.note.toNote
import com.learn.learnkmpnote.domain.time.DateTimeUtils

class SqlDelightNoteDataSource(
    db: NoteDatabase
) : NoteDataSource {

    private val queries = db.noteQueries

    override suspend fun insertNote(note: Note) {
        queries.insertNote(
            id = note.id,
            title = note.title,
            content = note.content,
            colorHex = note.colorHex,
            created = DateTimeUtils.toEpochMillis(note.created)
        )
    }

    override suspend fun getNoteById(id: Long): Note? {
        return queries.getNoteById(id)
            .executeAsOneOrNull()
            ?.toNote()
    }

    override suspend fun getAllNotes(): List<Note> {
        return queries.getAllNotes()
            .executeAsList()
            .map { it.toNote() }
    }

    override suspend fun deleteNoteById(id: Long) {
        queries.deleteNoteById(id)
    }

}