package com.learn.learnkmpnote.data.local.di

import com.learn.learnkmpnote.data.local.DatabaseDriverFactory
import com.learn.learnkmpnote.data.note.SqlDelightNoteDataSource
import com.learn.learnkmpnote.database.NoteDatabase
import com.learn.learnkmpnote.domain.note.NoteDataSource

class DatabaseModule {

    private val factory by lazy { DatabaseDriverFactory() }
    val noteDataSource: NoteDataSource by lazy {
        SqlDelightNoteDataSource(NoteDatabase(factory.createDriver()))
    }

}