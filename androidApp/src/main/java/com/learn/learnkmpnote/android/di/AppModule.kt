package com.learn.learnkmpnote.android.di

import android.app.Application
import app.cash.sqldelight.db.SqlDriver
import com.learn.learnkmpnote.data.local.DatabaseDriverFactory
import com.learn.learnkmpnote.data.note.SqlDelightNoteDataSource
import com.learn.learnkmpnote.database.NoteDatabase
import com.learn.learnkmpnote.domain.note.NoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideSqlDriver(app: Application): SqlDriver {
        return DatabaseDriverFactory(app).createDriver()
    }

    @Provides
    @Singleton
    fun provideNoteDataSource(driver: SqlDriver): NoteDataSource {
        return SqlDelightNoteDataSource(NoteDatabase(driver))
    }

}